#include "TagAlbumImageProvider.h"

#include <QDebug>
#include <QPainter>
#include <QUrlQuery>
#include <QFontDatabase>
#include <QFile>

#include "taglib.h"
#include "mpegfile.h"
#include "mpegproperties.h"
#include "attachedpictureframe.h"
#include "id3v2tag.h"
#include "id3v2header.h"
#include "id3v2frame.h"
#include "flacfile.h"
#include "oggfile.h"

TagAlbumImageProvider::TagAlbumImageProvider() : QQuickImageProvider(QQuickImageProvider::Image)
{
    QFontDatabase::addApplicationFont(":/Courgette-Regular.ttf");
}

QImage TagAlbumImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    if (!m_cache.contains(id))
    {
        m_cache[id] = createImage(id);
    }

    return m_cache.value(id);
}

QImage TagAlbumImageProvider::createImage(const QString &id)
{
    QUrl url(id);
    QUrl base(url);
    base.setQuery("");
    if (base.fileName().endsWith("mp3", Qt::CaseInsensitive))
    {
        if (!QFile::exists(base.toLocalFile()))
        {
            qWarning() << "No such file" << base.toLocalFile();
            return errorImage(url);
        }

        TagLib::MPEG::File f(base.toLocalFile().toLocal8Bit().constData());
        TagLib::ID3v2::Tag* tag = f.ID3v2Tag();
        TagLib::ID3v2::FrameList frameList;
        TagLib::ID3v2::AttachedPictureFrame *picFrame;

        if (!tag)
        {
            qDebug() << "No such tag" << id;
            return errorImage(url);
        }

        frameList = tag->frameListMap()["APIC"];

        if (frameList.isEmpty())
        {
            qDebug() << "No frame" << id;
            return errorImage(url);
        }

        for (auto it = frameList.begin(); it != frameList.end(); ++it)
        {
            picFrame = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(*it);

            auto img = QImage::fromData((const uchar*) picFrame->picture().data(), picFrame->picture().size());
            if (!img.isNull())
            {
                return img;
            }
        }

        qDebug() << "No mp3 cover art";
        return errorImage(url);
    }
    else if (base.fileName().endsWith("flac", Qt::CaseInsensitive))
    {
        TagLib::FLAC::File f(url.toLocalFile().toLocal8Bit().constData());
        for (TagLib::FLAC::Picture* p : f.pictureList())
        {
            auto img = QImage::fromData((const uchar*) p->data().data(), p->data().size());
            if (!img.isNull())
            {
                return img;
            }
        }

        qDebug() << "No flac cover art";
        return errorImage(url);
    }

    qDebug() << "unknown type" << id;
    return errorImage(url);
}

QImage TagAlbumImageProvider::errorImage(const QUrl& url)
{
    QImage img(QSize(256, 256), QImage::Format_ARGB32);

    static const QStringList colors {
        "#b56969",
        "#22264b",
        "#e6cf8b",
        "#c5d5cb",
        "#98dafc"
    };

    img.fill(QColor(colors.at(rand() % colors.length())));

    QPainter p;
    p.begin(&img);

    QUrlQuery q(url);
    QString album;
    if (q.hasQueryItem("album"))
    {
        album = q.queryItemValue("album");
    }
    else
    {
        album = url.toString();
    }

    p.setPen(QColor("white"));
    p.setFont(QFont("Courgette", 32));
    p.drawText(img.rect(), Qt::AlignCenter | Qt::TextWordWrap, album);

    return img;
}

