#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QPluginLoader>
#include <QMediaPlayer>
#include <QMediaService>
#include <QAudioDecoderControl>
#include <QMediaServiceProviderPlugin>
#include <QMediaServiceProviderFactoryInterface>

#include "ClementineDatabase.h"
#include "TagAlbumImageProvider.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("db", new ClementineDatabase);
    engine.addImageProvider("embedded", new TagAlbumImageProvider);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
