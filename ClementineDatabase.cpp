#include "ClementineDatabase.h"
#include <QUrl>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QStandardPaths>
#include <QDebug>
#include <QSqlError>

static const QStringList ALBUM_FIELDS {
    "album",
    "art_automatic",
    "filename",
    "effective_albumartist"
};

ClementineDatabase::ClementineDatabase(QObject *parent) : QObject(parent)
{
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    auto clementineDB = QString("%1/Clementine/clementine.db").arg(QStandardPaths::locate(QStandardPaths::HomeLocation, ".config", QStandardPaths::LocateDirectory));
    qDebug() << clementineDB;
    m_db.setDatabaseName(clementineDB);
    if (!m_db.open())
    {
        qCritical() << "Cannot open DB";
    }

    m_albums = new AlbumModel(this);
    m_albums->setQuery(QString("SELECT %1 from songs GROUP BY album, effective_albumartist ORDER BY RANDOM();").arg(ALBUM_FIELDS.join(", ")));
}

QVariantList ClementineDatabase::albumSongList(const QString &album, const QString& artist)
{
    QVariantList songs;
    QSqlQuery q("SELECT filename FROM songs WHERE album = ? AND artist = ?", m_db);
    q.addBindValue(album);
    q.addBindValue(artist);

    if (!q.exec())
    {
        qDebug() << "Couldn't fetch songs" << q.lastError().text() << q.executedQuery();
        return QVariantList();
    }

    while (q.next())
    {
        songs << q.value(0);
    }

    return songs;
}

QVariantMap ClementineDatabase::albumInfo(const QString &source)
{
    QVariantMap info;
    QSqlQuery q(QString("SELECT %1 FROM songs WHERE filename = ?").arg(ALBUM_FIELDS.join(", ")), m_db);
    q.addBindValue(QUrl(source).toEncoded());

    if (!q.exec())
    {
        qDebug() << "Couldn't fetch album" << q.lastError().text() << q.executedQuery();
        return QVariantMap();
    }

    if (!q.next())
    {
        qDebug() << "Can't find album" << q.executedQuery() << q.boundValues();
        return QVariantMap();
    }

    auto roles = m_albums->roleNames();
    for (int i = 0; i < ALBUM_FIELDS.length(); ++i)
    {
        info[roles[i]] = q.value(i);
    }

    return info;
}

QVariantList ClementineDatabase::allSongs()
{
    QVariantList songs;
    QSqlQuery q("SELECT filename FROM songs ORDER BY RANDOM();", m_db);

    if (!q.exec())
    {
        qDebug() << "Couldn't fetch songs" << q.lastError().text() << q.executedQuery();
        return QVariantList();
    }

    while (q.next())
    {
        songs << q.value(0);
    }

    return songs;
}

QHash<int, QByteArray> AlbumModel::roleNames() const
{
    return QHash<int, QByteArray>{
        {Qt::DisplayRole, "album"},
        {Qt::DisplayRole + 1, "art"},
        {Qt::DisplayRole + 2, "song_file"},
        {Qt::DisplayRole + 3, "artist"},
    };
}

QVariant AlbumModel::data(const QModelIndex &index, int role) const
{
    return QSqlQueryModel::data(QSqlQueryModel::index(index.row(), role - Qt::DisplayRole));
}


