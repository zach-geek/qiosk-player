import QtQuick 2.0
import QtGraphicalEffects 1.0

DropShadow {
    anchors.fill: source
    radius: 8
    samples: 17
    horizontalOffset: 5
    verticalOffset: 5
    color: "#aa000000"
}
