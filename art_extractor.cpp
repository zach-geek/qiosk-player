#include <QGuiApplication>
#include <QDebug>
#include <QCommandLineParser>
#include "TagAlbumImageProvider.h"
#include <QTime>

#include "iostream"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);


    srand(QTime::currentTime().msecsSinceStartOfDay());

    QCommandLineParser parser;
    parser.addPositionalArgument("query", "id query");

    QCommandLineOption output("o", "output", "PATH");
    parser.addOption(output);

    parser.process(app);

    TagAlbumImageProvider p;
    QSize s;
    QImage img = p.requestImage(parser.positionalArguments().first(), &s, QSize());

    if (img.isNull())
    {
        qCritical() << "Empty image";
        return -1;
    }

    if (!img.save(parser.value(output)))
    {
        qCritical() << "Couldn't save " << img;
        return -1;
    }

    return 0;
}
