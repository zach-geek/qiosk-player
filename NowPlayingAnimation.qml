import QtQuick 2.0

SequentialAnimation {
    id: root
    property var target
    property alias property: firstStep.property
    property alias properties: firstStep.properties
    property var from
    property var to

    running: true
    onStopped: start()

    function setRand() {
        firstStep.duration = Math.random() * 6000 + 3000
        secondStep.duration = Math.random() * 6000 + 3000
    }

    Component.onCompleted: setRand()

    ScriptAction {
        id: rand
        script: setRand()
    }

    NumberAnimation {
        id: firstStep
        target: root.target
        from: root.from
        to: root.to
        duration: 5000
        easing.type: Easing.InOutCubic
    }
    NumberAnimation {
        id: secondStep
        target: root.target
        property: root.property
        properties: root.properties
        from: root.to
        to: root.from
        duration: 5000
        easing.type: Easing.InOutCubic
    }
}
