import QtQuick 2.8
import QtQuick.Window 2.2
import QtMultimedia 5.8
import "."

Window {
    id: window
    visible: true
    width: 2560 * 0.9
    height: 1440 * 0.9
    title: qsTr("Qiosk Player")
    property bool fullscreen: false
    onFullscreenChanged: fullscreen ? window.showFullScreen() : window.showNormal()

    Shortcut {
        sequence: "F11"
        onActivated: window.fullscreen = !window.fullscreen
    }

    Shortcut {
        sequence: "Ctrl+Q"
        onActivated: Qt.quit()
    }

    Shortcut {
        sequence: "Esc"
        onActivated: Controller.currentPage = "AlbumView.qml"
    }

    Shortcut {
        sequence: "space"
        onActivated: Controller.player.playbackState === Audio.PausedState ? Controller.player.play() : Controller.player.pause()
    }

    Shortcut {
        sequence: "s"
        onActivated: Controller.shuffleAll()
    }

    Shortcut {
        sequence: "n"
        onActivated: Controller.currentPage = "NowPlayingView.qml"
    }

    Loader {
        anchors.fill: parent
        source: Controller.currentPage
    }
}
