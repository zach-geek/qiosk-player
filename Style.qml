pragma Singleton
import QtQuick 2.0

Item {
    property FontLoader titleFont: FontLoader {
        source: "qrc:/Courgette-Regular.ttf"
    }

    property FontLoader subTitleFont: FontLoader {
        source: "qrc:/Dosis-Regular.ttf"
    }

    property FontLoader monoFont: FontLoader {
        source: "qrc:/CutiveMono-Regular.ttf"
    }
}
