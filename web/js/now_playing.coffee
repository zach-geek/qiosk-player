class NowPlaying extends Page
  animation_duration: 2000
  constructor: ->
    super('#now-playing-page')
    Controller.on "artistChanged.NowPaying", => @updateInfo()

  updateInfo: ->
    $('.back-button').click -> Controller.goTo(AlbumView)
    info = await Util.json("/info/#{Controller.currentSong().key()}")
    data = if info.summary? then [info] else []
    s = d3.select('.now-playing-info')
          .selectAll('.artist-info')
          .data(data, (i) -> i.artist)

    s.enter().append("div")
     .attr('class', 'artist-info')
     .style('left', '-50em')
     .text((i) -> i.summary)
     .transition()
     .delay(2000)
     .duration(@animation_duration)
     .style('left', '0em')
     .on 'end', =>
       d3.select('.artist-info')
         .filter((d) -> d.artist is info.artist)
         .transition()
         .delay(8000)
         .duration(@animation_duration)
         .style('left', '-50em')

    s.exit()
     .transition()
     .duration(Util.animation_duration)
     .style('left', '-25em')
     .remove()


  keydown: ->
    switch d3.event.key
      when "Escape"
        Controller.goTo(AlbumView)
      when "ArrowRight"
        Controller.next()
      when "ArrowLeft"
        Controller.prev()
      else
        super()

window.NowPlaying = new NowPlaying
