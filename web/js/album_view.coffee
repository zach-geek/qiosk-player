class AlbumView extends Page
  constructor: -> super('#album-view-page')
  show: ->
    @load() unless @albums?
    super()

  play: (album) ->
    Controller.playAlbum(album)

  load: ->
    console.log "Loading"
    @albums = await Util.json("/albums.json")
    console.log "Loaded #{@albums.length} albums"
    Album.extend(a) for a in @albums
    @setCurrentAlbum(0)
    d3.selectAll('.album-background')
      .attr('src', @currentAlbum.cover_url())
    @update()

  setCurrentAlbum: (idx) ->
    @currentAlbumIdx = idx
    @currentAlbum = @albums[idx]
    album = @albums[idx]
    clearTimeout @interval if @interval?


    node = d3.selectAll('.album').filter((a) -> a.key() == album.key()).nodes()[0]
    return unless node?
    pos = $(node).position()

    scrollTop = $('.album-view')[0].scrollTop
    finalY =  scrollTop + pos.top

    d3.select('.album-view #highlight').transition().duration(Util.animation_duration)
      .style('left', pos.left + "px")
      .style('top', finalY + "px")
      .on 'end', =>
        @interval = Util.waitThen Util.animation_duration * 2, =>
          d3.selectAll('.album-background')
            .attr('src', @currentAlbum.cover_url())

    if finalY + $('.album img').height() > scrollTop + $('.album-view').height()
      d3.select('.album-view').transition().duration(Util.animation_duration)
        .tween "scrolltoptween", ->
          node = this
          i = d3.interpolateNumber(node.scrollTop, finalY)
          return (t) ->
            node.scrollTop = i(t)
    else if finalY < scrollTop
      d3.select('.album-view').transition().duration(Util.animation_duration)
        .tween "scrolltoptween", ->
          node = this
          i = d3.interpolateNumber(node.scrollTop, finalY - $('.album-view').height() + $('.album').height())
          return (t) ->
            node.scrollTop = i(t)

  update: ->
    s = d3.selectAll('.album-view')
          .selectAll('.album')
          .data(@albums, (a) -> "#{a.album}, #{a.effective_albumartist}")

    album = s.enter()
     .append('a')
     .attr('class', 'album')
     .attr('href', '#')
     .attr('draggable', false)
     .on('click', (a) => @play(a))

    album.append('img')
      .attr('src', (a) -> a.cover_url())
      .attr('title', (a) -> "#{a.album}\n#{a.effective_albumartist}")
      .attr('alt', (a) -> "#{a.album}\n#{a.effective_albumartist}")
      .attr('draggable', false)

  keydown: ->
    switch d3.event.key
      when "ArrowRight"
        idx = @currentAlbumIdx + 1
        @setCurrentAlbum(idx) if idx < @albums.length
      when "ArrowLeft"
        idx = @currentAlbumIdx - 1
        @setCurrentAlbum(idx) if idx >= 0
      when "ArrowDown"
        idx = @currentAlbumIdx + Math.floor($('.album-view').width() / $('.album').width())
        @setCurrentAlbum(idx) if idx < @albums.length
      when "ArrowUp"
        idx = @currentAlbumIdx - Math.floor($('.album-view').width() / $('.album').width())
        @setCurrentAlbum(idx) if idx >= 0
      when "Enter", "Return"
        @play(@currentAlbum)
      when "Escape"
        Controller.goTo(NowPlaying) if Controller.currentAlbum?
      when "s"
        Controller.shuffle()
      when "p"
        Controller.party()
      else
        super()


window.AlbumView = new AlbumView
