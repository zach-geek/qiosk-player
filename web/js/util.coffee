class Util
  animation_duration: 256
  urlEncode: (s) ->
    encodeURIComponent(btoa(s))
  waitThen: (i, c) ->
    setTimeout(c, i)
  json: (path) ->
    return new Promise (resolve, reject) ->
      d3.json path, (err, data) ->
        return reject(err) if err?
        resolve(data)
  delay: (ms) ->
    return new Promise (resolve) =>
      setTimeout(resolve, ms)

window.Util = new Util
window._u = window.Util


Album =
  extend: (obj) ->
    obj[k] = v for k,v of Album
    obj

  key: -> "#{_u.urlEncode(@effective_albumartist)}/#{_u.urlEncode(@album)}"
  cover_url: -> "/cover/#{@key()}"
  music_url: -> "/music#{(new URL(@filename)).pathname}"

Song = Album

window.Album = Album
window.Song = Song
