class Page
  constructor: (@element) ->
    Controller.pages.push this

  hide: ->
    d3.select(@element).classed('not-shown', true)

  show: ->
    d3.select('body').on("keydown", => @keydown())
    d3.select(@element).classed('not-shown', false)

  keydown: ->
    switch d3.event.key
      when " "
        Controller.playPause()
      else
        console.log(d3.event.key)

window.Page = Page
