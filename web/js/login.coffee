class Login extends Page
  constructor: ->
    super('#login-page')
    $(document).ready -> $('#login').click -> location.href = "/auth/gitlab"
    $.get '/logged_in', (d, e) ->
      Controller.goTo(AlbumView)

window.Login = new Login
