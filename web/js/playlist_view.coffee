class PlaylistView extends Page
  constructor: -> super('#playlist-view-page')
  show: ->
    @load() unless @artists
    super()

  load: ->
    @artists = await Util.json("/artists.json")
    Album.extend(a) for a in @artists

    s = d3.select('.playlist-view').selectAll('.artist').data(@artists, (a) -> a.effective_albumartist)

    s.enter().append('div')
     .attr('class', 'artist')
     .text((a) -> a.effective_albumartist)

window.PlaylistView = new PlaylistView
