class Controller
  pages: []
  playlistIndex: 0
  currentSong: -> @playlist[@playlistIndex]
  player: -> $('#player')[0]
  on: (t, h) -> @dispatch.on(t,h)

  constructor: ->
    c = this
    @dispatch = d3.dispatch('songChanged', 'artistChanged')

    $(document).ready =>
      d3.select('#player').on 'ended', -> c.next()
      d3.select('#player').on 'error', ->
        console.error "Skipping player error"
        c.next()
      d3.select('#player').on 'timeupdate', ->
        d3.selectAll('.current-playback-time').text "#{@currentTime.toFixed(0)} / #{@duration.toFixed(0)} seconds"
      d3.select('#player').on 'canplaythrough', =>
        waitTime = new Date().getTime() - @startLoadingTime
        console.log("Took #{waitTime} ms. Sleeping that much")
        Util.delay(waitTime).then =>
          console.log("Slept. Total", new Date().getTime() - @startLoadingTime, @canPlayThrough, @waitingToPlay)
          @canPlayThrough = true
          @player().play() if @waitingToPlay
          @waitingToPlay = false

  goTo: (page) ->
    p.hide() for p in @pages
    page.show()

  playPause: () ->
    if @player().paused
      if @canPlayThrough
        @player().play()
      else
        @waitingToPlay = true
    else @player().pause()

  stop: ->
    @waitingToPlay = false
    @playlistIndex = 0
    @playlist = []
    @goTo(AlbumView)

  next: ->
    @playlistIndex += 1
    return @stop() if @playlistIndex >= @playlist.length
    @updateSong()

  prev: ->
    @playlistIndex -= 1
    return @stop() if @playlistIndex < 0
    @updateSong()

  playAlbum: (album) ->
    @playlist = await Util.json("/songs/#{album.key()}")
    Song.extend(a) for a in @playlist
    @playlistIndex = 0

    @updateSong()
    @goTo(NowPlaying)

  retrievePlaylist: (path) ->
    @playlist = await Util.json(path)
    Song.extend(a) for a in @playlist
    @playlistIndex = 0

    @updateSong()
    @goTo(NowPlaying)

  shuffle: -> @retrievePlaylist("/shuffle.json")
  party: -> @retrievePlaylist("/party.json")

  updateAlbum: (album) ->
    @currentAlbum = album
    d3.selectAll('.current-album-title').text(album.album)
    d3.selectAll('.current-album-artist').text(album.effective_albumartist)
    d3.selectAll('.current-album-cover').attr('src', album.cover_url())

  updateSong: ->
    oldAlbum = @currentAlbum
    @updateAlbum(@currentSong())
    d3.selectAll('.current-song-title').text(@currentSong().title)
    d3.selectAll('.current-playback-index').text("#{@playlistIndex + 1} / #{@playlist.length} songs")

    @canPlayThrough = false
    @waitingToPlay = true
    @startLoadingTime = new Date().getTime()
    $('#player').attr('src', @currentSong().music_url())

    @dispatch.apply("songChanged")
    @dispatch.apply("artistChanged") unless oldAlbum?.effective_albumartist is @currentSong().effective_albumartist

window.Controller = new Controller
