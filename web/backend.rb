require 'sinatra'
require 'sinatra/reloader'
require 'coffee-script'
require 'slim'
require 'slim/include'
Bundler.require
require 'wikipedia'
load 'config.rb'

environment = Sprockets::Environment.new
environment.append_path 'style'
environment.append_path 'js'

DB = Sequel.connect(DATABASE_URL)

ALBUM_FIELDS = %i[
  album
  art_automatic
  art_manual
  filename
  effective_albumartist
  track
]

SONG_FIELDS = ALBUM_FIELDS + %i[
  title
  genre
]

logger = Logger.new(STDERR)
logger.level = Logger::DEBUG

use OmniAuth::Builder do
  provider :gitlab, GITLAB_OATH2::APP_ID, GITLAB_OATH2::SECRET, scope: 'read_user openid', redirect_url: 'https://io.zachcapalbo.com/auth/gitlab/callback'
end

set :bind, '0.0.0.0'
enable :sessions

def decode(str)
  Base64.decode64(URI.decode(str))
end

before do
  pass if request.path_info.start_with?("/auth/")
  pass if ["/", "/js/app.js", "/style/app.css"].include?(request.path_info)
  pass if request.ip == '127.0.0.1' or request.ip.start_with?("192.168")
  halt 403 unless session[:authorized] == true
end

get '/js/app.js' do
  content_type 'application/javascript'
  logger.debug environment.inspect
  environment['app.js']
end

get '/style/app.css' do
  content_type 'text/css'
  environment['app.css']
end

get '/' do
  slim :main
end

get '/auth/:provider/callback' do
  auth = request.env['omniauth.auth']
  halt 403 unless ALLOWED_USERS.include?(auth["info"]["email"])
  session[:authorized] = true

  redirect "/"
end

get '/logged_in' do
  "true"
end

get '/albums.json' do
  content_type 'text/json'
  DB[:songs]
    .select(*ALBUM_FIELDS)
    .exclude(album: "")
    .exclude(effective_albumartist: "")
    .group_by(:album, :effective_albumartist)
    .order_by(Sequel.lit("RANDOM()")).all.to_json
end

get '/artists.json' do
  content_type 'text/json'
  DB[:songs]
    .select(*ALBUM_FIELDS, Sequel.lit("count(album)"))
    .exclude(album: "")
    .exclude(effective_albumartist: "")
    .group_by(:effective_albumartist)
    .order_by(Sequel.lit("RANDOM()")).all.to_json
end

# TODO: Use mp3info gem
ART_EXTRACTOR = "e:/src/qml/build-QioskPlayer-Desktop_Qt_5_9_1_MSVC2017_64bit-Release with Debug Information/art_extractor.exe"

get '/cover/:artist/:album' do
  album = decode(params[:album])
  artist = decode(params[:artist])
  album = DB[:songs].select(*ALBUM_FIELDS).where(album: album, effective_albumartist: artist).first
  album ||= {album: "<Unnamed Album>", filename: ""}
  art = album[:art_automatic] || album[:art_manual]

  path = art
  # path = `cygpath -u "#{path}"`.strip if OS.cygwin?

  if art == "(embedded)" or art.nil? or !File.exist?(path) then
    path = "album_cache/#{Base64.urlsafe_encode64("#{album[:album]}-#{album[:effective_albumartist]}")}.jpg"
    path.gsub!(/[^\w\/\.]/, '')
    logger.debug("Checking for #{path}: #{File.exist?(path)}")
    unless File.exist?(path) then
      id = "#{album[:filename]}?album=#{URI.encode(album[:album])}"
      cmd = [ART_EXTRACTOR, id, "-o", path]
      logger.debug("Running #{cmd}")
      raise StandardError.new("Couldn't extract image: #{$?.exitstatus} (#{ENV["SHELL"]})\n\n#{cmd}\n\n") unless system(*cmd)
    end
    send_file path
  else
    send_file path
  end
end

get '/songs/:artist/:album' do
   DB[:songs]
    .select(*SONG_FIELDS)
    .where(album: decode(params[:album]), effective_albumartist: decode(params[:artist]))
    .order_by(:track)
    .all.to_json
end

get '/songs/:field' do
  field = decode(params[:field])
  DB[:songs].select(*SONG_FIELDS).where(Sequel.|({album: field}, {effective_albumartist: field})).all.to_json
end

get '/info/:artist/:album' do
  artist = decode(params[:artist])
  page = Wikipedia.find(artist)

  if page.summary.nil? then
    potential_artists = artist.split(/\band\b|[\&\,\/\(\+\;]|\bfeat\.?\b|\bfeaturing\b/i).map(&:strip).shuffle
    logger.debug potential_artists.inspect

    artist, ppage = potential_artists.lazy.map do |pa|
      pa_page = Wikipedia.find(pa)
      [pa, pa_page]
    end.find {|pa, pa_page| !pa_page.summary.nil?}

    page = ppage unless ppage.nil?
  end

  return {
    artist: artist,
    summary: page.summary
  }.to_json
end

get '/shuffle.json' do
    content_type 'text/json'

  DB[:songs].select(*SONG_FIELDS).order_by(Sequel.lit("RANDOM()")).all.to_json
end

get '/party.json' do
  content_type 'text/json'

  # Filter out songs that don't really go over well as party music... YMMV
  DB[:songs]
    .select(*SONG_FIELDS)
    .where(Sequel.lit(%|length < 963000000000 AND genre != "Classical" and effective_albumartist not like "%Monks%"|))
    .exclude(album: "")
    .exclude(effective_albumartist: "")
    .order_by(Sequel.lit("RANDOM()"))
    .all.to_json
end

get '/music/*' do
  path = params[:splat].first
  logger.debug "splatted #{path}"
  send_file path
  # send_file `cygpath -u "#{path}"`.strip
end
