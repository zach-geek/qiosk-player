# - Try to find the Taglib library
# Once done this will define:
#
#  TAGLIB_FOUND - system has the taglib library
#  TAGLIB_CFLAGS - the taglib cflags
#  TAGLIB_LIBRARIES - the libraries needed to use TagLib
#  TAGLIB_LIBRARIES_DIRS - the directories the TagLib libraries reside in
#
# Copyright (c) 2013, Bart van der Velden, <bart@muckingabout.eu>
# Inspired by FindTagLib.cmake from the Amarok project which is Copyright (c) 2006, Laurent Montel,
# <montel@kde.org>
#
# Redistribution and use is allowed according to the terms of the BSD license.
#
# This script works if the taglib-config or taglib-config.cmd script can be found in the path, or
# if a TAGLIB_ROOT environment variable is set.
#
# Currently it is tested on my own machines on Linux with gcc and on Windows with Microsoft Visual
# Studio 2010. The intention is to also make it work with clang on Linux and MinGW on Windows.

# Set the minimum version in case it has not been done earlier
if(NOT TAGLIB_MIN_VERSION)
  set(TAGLIB_MIN_VERSION "1.11")
endif(NOT TAGLIB_MIN_VERSION)

if(WIN32)
  set(TAGLIB_CONFIG_SCRIPT "taglib-config.cmd")
else(WIN32)
  set(TAGLIB_CONFIG_SCRIPT "taglib-config")
endif(WIN32)

# Try to find the taglib-config.comd (Windows) or taglib-config (non-Windows) script, which is
# installed when building and installing TagLib from source.
# For this to work the directory the script is in should be in the path. On Linux this is
# automatically the case when TagLib is built with the default prefix of /usr/local.
# On Windows it helps to add the TagLib bin directory to the path.
find_program(TAGLIBCONFIG_EXECUTABLE NAMES ${TAGLIB_CONFIG_SCRIPT} PATHS ${BIN_INSTALL_DIR} "e:/src/cmake/bin/")

if (NOT TAGLIBCONFIG_EXECUTABLE)
  # Could not find the taglib config executable. Our next hope is that the TAGLIB_ROOT environment
  # variable is set
  if(NOT Taglib_FIND_QUIETLY)
    message(STATUS "Try to find via TAGLIB_ROOT")
  endif()

  if (NOT "$ENV{TAGLIB_ROOT}" STREQUAL "")
    set(TAGLIB_BIN_PATH $ENV{TAGLIB_ROOT})
    if(WIN32)
      set(TAGLIB_BIN_PATH "${TAGLIB_BIN_PATH}\\bin")
    endif(WIN32)

    find_program(TAGLIBCONFIG_EXECUTABLE NAMES ${TAGLIB_CONFIG_SCRIPT} PATHS ${TAGLIB_BIN_PATH})

  endif()
endif()

#reset vars
set(TAGLIB_LIBRARIES)
set(TAGLIB_CFLAGS)

# if taglib-config has been found
if(TAGLIBCONFIG_EXECUTABLE)

  # Retrieve the version number
  exec_program(${TAGLIBCONFIG_EXECUTABLE}
    ARGS --version
    RETURN_VALUE _return_VALUE
    OUTPUT_VARIABLE TAGLIB_VERSION)

  # Do we have at least the minimum version?
  if(TAGLIB_VERSION STRLESS "${TAGLIB_MIN_VERSION}")

    message(STATUS "TagLib version too old: version searched :${TAGLIB_MIN_VERSION},"
      " found ${TAGLIB_VERSION}")
    set(TAGLIB_FOUND FALSE)

  else(TAGLIB_VERSION STRLESS "${TAGLIB_MIN_VERSION}")

    if(WIN32)
      # On Windows we cannot use the output from the taglib-config.cmd script for Visual C++ since
      # it is in the wrong format. Instead we retrieve the library prefix and deduce from there
      exec_program(${TAGLIBCONFIG_EXECUTABLE}
        ARGS --prefix
        RETURN_VALUE _return_VALUE
        OUTPUT_VARIABLE TAGLIB_PREFIX)

      set(TAGLIB_CFLAGS "${TAGLIB_PREFIX}/include/taglib")

    else(WIN32) # Only Linux for now

      # Retrieve the library string in the form: -L/usr/local/lib -ltag
      exec_program(${TAGLIBCONFIG_EXECUTABLE}
        ARGS --prefix
        RETURN_VALUE _return_VALUE
        OUTPUT_VARIABLE TAGLIB_PREFIX)

      # Retrieve the include path in the form: -I/usr/local/include/taglib
      exec_program(${TAGLIBCONFIG_EXECUTABLE}
        ARGS --cflags
        RETURN_VALUE _return_VALUE
        OUTPUT_VARIABLE TAGLIB_CFLAGS)

    endif(WIN32)

    # Find the TagLib library
    set(TAGLIB_LIBRARIES_DIRS "${TAGLIB_PREFIX}/lib")
    find_library(TAGLIB_LIBRARIES NAMES tag HINTS ${TAGLIB_LIBRARIES_DIRS})

    # Set to found if we have settings for include files and libraries
    if(TAGLIB_LIBRARIES AND TAGLIB_CFLAGS)
      set(TAGLIB_FOUND TRUE)
    endif()

    string(REGEX REPLACE " *-I" ";" TAGLIB_INCLUDES "${TAGLIB_CFLAGS}")

  endif(TAGLIB_VERSION STRLESS "${TAGLIB_MIN_VERSION}")

  mark_as_advanced(TAGLIB_CFLAGS TAGLIB_LIBRARIES TAGLIB_INCLUDES)

endif(TAGLIBCONFIG_EXECUTABLE)

if(TAGLIB_FOUND)
  if(NOT Taglib_FIND_QUIETLY)
    message(STATUS "Taglib version        : ${TAGLIB_VERSION}")
    message(STATUS "TAGLIB_INCLUDES       : ${TAGLIB_INCLUDES}")
    message(STATUS "TAGLIB_LIBRARIES      : ${TAGLIB_LIBRARIES}")
    message(STATUS "TAGLIB_LIBRARIES_DIRS : ${TAGLIB_LIBRARIES_DIRS}")
  endif(NOT Taglib_FIND_QUIETLY)
else(TAGLIB_FOUND)
  if(Taglib_FIND_REQUIRED)
    message(FATAL_ERROR "Could not find Taglib")
  endif(Taglib_FIND_REQUIRED)
endif(TAGLIB_FOUND)
