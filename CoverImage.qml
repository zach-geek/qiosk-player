import QtQuick 2.0
import "."

Image {
    id: cover
    property var album: Controller.currentAlbum
    width: 256
    height: 256
    source: {
        if (album.art === "(embedded)")
        {
            return "image://embedded/" + album.song_file + "?album=" + album.album
        }

        if (album.art.length > 0)
        {
            return "file:///" + album.art
        }

        return "image://embedded/err" + "?album=" + album.album
    }
}
