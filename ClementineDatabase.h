#ifndef CLEMENTINEDATABASE_H
#define CLEMENTINEDATABASE_H

#include <QObject>
#include <QSqlQueryModel>

class AlbumModel : public QSqlQueryModel
{
    Q_OBJECT

public:
    AlbumModel(QObject* parent = nullptr) : QSqlQueryModel(parent) {}

    virtual QHash<int, QByteArray>	roleNames() const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
};


class ClementineDatabase : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QSqlQueryModel* albums READ albums CONSTANT)
public:
    explicit ClementineDatabase(QObject *parent = nullptr);

    QSqlQueryModel* albums() { return m_albums; }
    Q_INVOKABLE QVariantList albumSongList(const QString &album, const QString& artist);
    Q_INVOKABLE QVariantMap albumInfo(const QString &source);
    Q_INVOKABLE QVariantList allSongs();
signals:

public slots:

private:
    QSqlQueryModel* m_albums;
    QSqlDatabase m_db;
};

#endif // CLEMENTINEDATABASE_H
