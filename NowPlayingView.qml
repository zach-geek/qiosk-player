import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

Item {
    focus: true
    Component.onCompleted: forceActiveFocus()
    Keys.onRightPressed: Controller.player.playlist.next()
    Keys.onLeftPressed: Controller.player.playlist.previous()

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.BlankCursor
    }

    CoverImage {
        id: bg
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }

    FastBlur {
        id: blur
        anchors.fill: bg
        source: bg
        radius: 54 * (desaturate.scale - 1.08) / (1.5 - 1.08) + 10
        transparentBorder: true
    }

    Rectangle {
        anchors.fill: parent
        color: "black"
    }

    Desaturate {
        id: desaturate
        source: blur
        width: bg.width
        height: bg.height
        desaturation: 0.55 * (1.0 - (desaturate.scale - 1.08) / (1.5 - 1.08))
        transformOrigin: Item.Center

        NowPlayingAnimation {
            target: desaturate
            property: "scale"
            from: 1.08
            to: 1.5
        }

        NowPlayingAnimation {
            target: desaturate
            property: "x"
            from: - bg.width * 0.05
            to: bg.width * 0.05
        }

        NowPlayingAnimation {
            target: desaturate
            property: "y"
            from: - bg.height * 0.05
            to: bg.height * 0.05
        }
    }

    Rectangle {
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: Math.max(parent.height * 0.15, infoColumn.height)
        color: "#aa312c32"

        RectangularGlow {
            anchors.fill: parent
            color: parent.color
            glowRadius: 10
            spread: 0.2
        }

        CoverShadow {
            source: cover
        }

        CoverImage {
            id: cover
            anchors.left: parent.left
            anchors.margins: width * 0.2
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height * 0.85
            width: height
        }

        Column {
            id: infoColumn
            anchors.left: cover.right
            anchors.margins: cover.anchors.margins

            Text {
                font.family: Style.titleFont.name
                font.pixelSize: 50
                color: "white"
                text: Controller.player.metaData.albumTitle
            }

            Text {
                font.family: Style.subTitleFont.name
                font.pixelSize: 32
                color: "white"
                text: {
                    var check = ["albumArtist", "author", "composer"]
                    for (var i in check)
                    {
                        var t = check[i]
                        if (Controller.player.metaData[t] !== undefined && Controller.player.metaData[t].toString().length > 0)
                        {
                            return Controller.player.metaData[t]
                        }
                    }
                }
            }

            Text {
                font.family: Style.subTitleFont.name
                font.pixelSize: 48
                color: "white"
                text: Controller.player.metaData.title
            }
        }

        Column {
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: cover.anchors.margins
            Text {
                anchors.right: parent.right
                font.family: Style.monoFont.name
                font.pixelSize: 32
                color: "white"
                horizontalAlignment: Text.AlignRight
                text: qsTr("%1/%2 seconds").arg((Controller.player.position / 1000).toFixed(0))
                                             .arg((Controller.player.duration / 1000).toFixed(0))
            }
            Text {
                anchors.right: parent.right
                font.family: Style.monoFont.name
                font.pixelSize: 32
                color: "white"
                horizontalAlignment: Text.AlignRight
                text: qsTr("%1/%2 songs").arg(Controller.player.playlist.currentIndex + 1)
                                         .arg(Controller.player.playlist.itemCount)
            }
        }
    }
}
