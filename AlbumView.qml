import QtQuick 2.0
import QtGraphicalEffects 1.0
import "."

Item {
    Image {
        id: bg
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }

    FastBlur {
        id: blur
        anchors.fill: bg
        source: bg
        radius: 64
    }

    Desaturate {
        source: blur
        anchors.fill: bg
        desaturation: 0.5
    }

    GridView {
        id: grid
        anchors.fill: parent
        model: db.albums
        cellWidth: 256 * 1.3
        cellHeight: 256 * 1.3

        layer.enabled: true
        layer.mipmap: true

        Component.onCompleted: forceActiveFocus()

        highlight: Component {
            Rectangle {
                color: "#98dafc"
                opacity: 0.2

                RectangularGlow {
                    anchors.fill: parent
                    color: parent.color
                    glowRadius: 10
                    spread: 0.2
                }
            }
        }

        delegate: Component {
            Item {
                id: delegate
                width: grid.cellWidth
                height: grid.cellHeight

                function play() {
                    Controller.playAlbum(model)
                }

                CoverShadow {
                    source: cover
                }

                CoverImage {
                    id: cover
                    anchors.centerIn: parent
                    album: model
                    Binding {
                        target: bg
                        property: "source"
                        value: cover.source
                        when: index == grid.currentIndex
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        grid.currentIndex = index
                        delegate.play()
                    }
                }

                Keys.onReturnPressed: delegate.play()
            }
        }
    }
}
