#ifndef TAGALBUMIMAGEPROVIDER_H
#define TAGALBUMIMAGEPROVIDER_H

#include <QQuickImageProvider>

class TagAlbumImageProvider : public QQuickImageProvider
{
public:
    TagAlbumImageProvider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;

private:
    QImage createImage(const QString& id);
    QImage errorImage(const QUrl &url);
    QHash<QString, QImage> m_cache;
};

#endif // TAGALBUMIMAGEPROVIDER_H
