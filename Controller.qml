pragma Singleton
import QtQuick 2.0
import QtMultimedia 5.8

Item {
    id: root
    property Audio player: Audio {
        audioRole: Audio.MusicRole
        onSourceChanged: console.log("Player source", source)
        onError: console.error("Player error", source, error, errorString)
        playlist: Playlist {
        }
        onStopped: root.currentPage = "AlbumView.qml"
    }

    property string currentPage: "AlbumView.qml"
    property var currentAlbum: []

    function playAlbum(album)
    {
        player.playlist.clear()
        player.playlist.addItems(db.albumSongList(album.album, album.artist))
        player.play()

        currentPage = "NowPlayingView.qml"
    }

    function shuffleAll()
    {
        player.playlist.clear()
        player.playlist.addItems(db.allSongs())
        player.play()
        currentPage = "NowPlayingView.qml"
    }

    Connections {
        target: player.playlist
        onCurrentItemSourceChanged: {
            var ai =  db.albumInfo(player.playlist.currentItemSource)
            root.currentAlbum = ai
        }
    }
}
